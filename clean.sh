#!/bin/bash
set -e

if [ -d ./build ]; then
  echo 'removing ./build directory'
  rm -rf ./build
fi
