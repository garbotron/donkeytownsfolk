#!/bin/bash
set -e

echo "compiling client TS to JS"
./node_modules/webpack/bin/webpack.js --mode development --config ./src/client/webpack.config.js

echo "compiling server TS to JS"
./node_modules/webpack/bin/webpack.js --mode development --config ./src/server/webpack.config.js

echo "copying static files to client build dir"
cp ./src/client/static/* ./build/client/

echo "build complete"
