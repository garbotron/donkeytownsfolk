# [donkeytownsfolk](https://garbotron.gitlab.io/donkeytownsfolk/)

Web App for tracking price-limited Magic the Gathering decks, written with [serverless](https://serverless.com/) and [TypeScript](https://www.typescriptlang.org/).

[![build status](https://gitlab.com/garbotron/donkeytownsfolk/badges/master/build.svg)](https://gitlab.com/garbotron/donkeytownsfolk/commits/master)

Average prices are gathered from [MTGStocks.com](http://www.mtgstocks.com/). These prices are used for all cards, except basic lands which are free.

## Architecture

Both the server and client are written in [TypeScript](https://www.typescriptlang.org/). The server source is located under `/src/server`, the client is located under `/src/client` and any shared code (e.g. the data model) is located under `/src/common`.

The server uses the [Serverless](https://serverless.com/) framework to provide simple HTTP function endpoints that ultimately access a [MongoDB](https://www.mongodb.com/) instance.

The client is a single-page application (SPA) written with [React](https://facebook.github.io/react/). It uses *hash routing*, meaning that the URL component after the hash symbol (`#`) represents the current "page". This way the site can act very similarly to traditional web servers, even though there's only one root HTML page.

Both the client and server builds make extensive use of [TSLint](https://github.com/palantir/tslint) with a customized (superior) ruleset.

## Environment

The following environment variables must be set before you can build/deploy the site.

```
DONKEYTOWNSFOLK_MONGODB_URI                  Connection URI to your MongoDB instance (includes user/password)
DONKEYTOWNSFOLK_MONGODB_COLLECTION_PRICES    Name of the table that will contain current card prices
DONKEYTOWNSFOLK_MONGODB_COLLECTION_USERS     Name of the table that will contain your users, their decks, etc
AWS_ACCESS_KEY_ID                            AWS access key ID (for Serverless deploy)
AWS_SECRET_ACCESS_KEY                        AWS access key (for Serverless deploy)
```

## Installation

All of the build/install steps are managed by [npm](https://www.npmjs.com/).

```
npm run build     run the client and server builds (producing output in /build)
npm run clean     clean all build artifacts
npm install       download all dependencies and run full build
```

All output artifacts are produced in the `/build` directory.

```
/build/server     JavaScript (ES5) that export the Serverless endpoint functions
/build/client     The full client website (HTML, JavaScript, etc)
```

## Deployment

To deploy the server, just run `npm run deploy`. That's all it takes.

The client is just static HTML - all you need to do is copy it to a fileserver. The GitLab CI script copies the site to public (free) GitLab pages hosting.

## Test

I'm a terrible software developer and as such there are no tests. Seriously, though, it's very tough to write automated tests for a client-heavy website that's heavily reliant on cloud services. It's just not worth my time right now.

## Why "Donkey Townsfolk"?

![Gatherer card: "Cheap Ass"](http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=74220&type=card)
