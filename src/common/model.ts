// ===== Generic shared model types / functions =====

export enum FindResult {
    Found,
    NotFound,
}

export type LoginToken = string

export interface CardPrice {
    name: string
    price: number
}

export interface CardPriceResult extends CardPrice {
    result: FindResult
}

export interface CardPriceDbEntry extends CardPrice {
    key: string // name in all lowercase with all non-alphanumeric characters trimmed out
}

export interface Card {
    name: string
    count: number
    lastScanPrice: number | 'NOT_FOUND'
}

export interface Deck {
    name: string
    isPrivate: boolean
    lastScanPrice: number
    lastScanDate: number // convert to Date using 'new Date(number)'
    cards: Card[]
    sideboard: Card[]
    commander: Card | undefined
    notes: string
}

export interface User {
    name: string
    passwordHash: string
    tokenHash: string
    decks: Deck[]
}

export interface DeckWithUser {
    user: string
    deck: Deck
}

export function cardNameToKey(name: string) {
    let key = ''
    for (const c of name) {
        if (c.match(/^[a-z0-9_-]+$/i)) {
            key += c.toLowerCase()
        }
    }
    return key
}

// ===== Function input/output type declarations =====
// Disabling the empty interface rule for this section since some of the I/O types will be empty but are still useful.
// tslint:disable:no-empty-interface

export interface PricesQueryInput {
    cardKeys: string[]
}

export interface PricesQueryOutput {
    results: CardPriceResult[]
}

export interface PricesScrapeInput {
    // nothing needed
}

export interface PricesScrapeOutput {
    // nothing needed
}

export interface UserLoginInput {
    name: string
    password: string
}

export interface UserLoginOutput {
    token: LoginToken
}

export interface UserCreateInput {
    name: string
    password: string
}

export interface UserCreateOutput {
    token: LoginToken
}

export interface UserDeleteInput {
    name: string
    token: LoginToken
    password: string
}

export interface UserDeleteOutput {
    // nothing needed
}

export interface UserUpdateInput {
    name: string
    token: LoginToken
    password?: string
    removeDeckNames?: string[]
    addDecks?: Deck[]
}

export interface UserUpdateOutput {
    // nothing needed
}

export interface DecksQueryInput {
    loggedInUser?: {
        name: string,
        token: LoginToken,
    }
    filterUser?: string,
    filterQuery?: string
}

export interface DecksQueryOutput {
    decks: DeckWithUser[]
}
