export const dtApiRoot = 'https://6ffpxjh5a3.execute-api.us-east-1.amazonaws.com/dev'

export function goToHash(hashLocation: string) {
    if (window.location.hash === `#${hashLocation}`) {
        window.dispatchEvent(new HashChangeEvent('hashchange'))
    } else {
        window.location.href = `#${hashLocation}`
    }
}
