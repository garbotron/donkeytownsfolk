import * as $ from 'jquery'
import * as React from 'react'
import * as dt from '../../common/model'
import { dtApiRoot, goToHash } from '../common'

export interface MenuProps {
    loggedInUser: string | undefined
    apiToken: dt.LoginToken | undefined
    logout: () => void
    login: (user: string, token: dt.LoginToken) => void
    startLoading: (text: string) => void
    stopLoading: () => void
    showError: (text: string) => void
}

interface MenuState {
    searchText: string
    editUser: string
    password: string
    deckName: string
    deckIsPrivate: boolean
}

interface ModalForm {
    modal: HTMLElement | undefined
    form: HTMLElement | undefined
}

export class MenuComponent extends React.Component<MenuProps, MenuState> {
    private loginMf: ModalForm = { modal: undefined, form: undefined }
    private createUserMf: ModalForm = { modal: undefined, form: undefined }
    private changePasswordMf: ModalForm = { modal: undefined, form: undefined }
    private deleteUserMf: ModalForm = { modal: undefined, form: undefined }
    private newDeckMf: ModalForm = { modal: undefined, form: undefined }

    constructor(props: MenuProps) {
        super(props)
        this.state = { searchText: '', editUser: '', password: '', deckName: '', deckIsPrivate: false }
    }

    public componentDidMount() {
        this.setupMf(this.loginMf, () => this.login(), {
            name: {
                identifier: 'username',
                rules: [{
                    type: 'empty',
                    prompt: 'Please enter a username',
                }],
            },
            password: {
                identifier: 'password',
                rules: [{
                    type: 'empty',
                    prompt: 'Please enter a password',
                }],
            },
        })

        this.setupMf(this.createUserMf, () => this.createUser(), {
            name: {
                identifier: 'username',
                rules: [{
                    type: 'empty',
                    prompt: 'Please enter a username',
                }],
            },
            password: {
                identifier: 'password',
                rules: [{
                    type: 'empty',
                    prompt: 'Please enter a password',
                }],
            },
            passconfirm: {
                identifier: 'passconfirm',
                rules: [{
                    type: 'match[password]',
                    prompt: 'Your passwords don\'t match',
                }],
            },
        })

        this.setupMf(this.changePasswordMf, () => this.changePassword(), {
            password: {
                identifier: 'password',
                rules: [{
                    type: 'empty',
                    prompt: 'Please enter a password',
                }],
            },
            passconfirm: {
                identifier: 'passconfirm',
                rules: [{
                    type: 'match[password]',
                    prompt: 'Your passwords don\'t match',
                }],
            },
        })

        this.setupMf(this.deleteUserMf, () => this.deleteUser(), {
            password: {
                identifier: 'password',
                rules: [{
                    type: 'empty',
                    prompt: 'Please enter a password',
                }],
            },
        })

        this.setupMf(this.newDeckMf, () => this.createNewDeck(), {
            name: {
                identifier: 'name',
                rules: [{
                    type: 'empty',
                    prompt: 'Please enter a deck name',
                }],
            },
        })
    }

    public render() {
        return <div className='row'>
            <div className='column'>
                <div className='ui menu'>
                    {this.renderSearchMenuItem()}
                    {this.props.loggedInUser && this.renderLoggedInRightMenu()}
                    {!this.props.loggedInUser && this.renderNotLoggedInRightMenu()}
                    {this.renderLoginModal()}
                    {this.renderUserModal()}
                    {this.renderChangePasswordModal()}
                    {this.renderDeleteUserModal()}
                    {this.renderNewDeckModal()}
                </div>
            </div>
        </div>
    }

    private renderSearchMenuItem() {
        const loc = `/search/${encodeURIComponent(this.state.searchText)}`
        return <div className='item'>
            <div className='ui transparent icon input'>
                <input
                    name='search'
                    type='text'
                    placeholder='Search decks...'
                    value={this.state.searchText}
                    onChange={(e) => this.setState({ searchText: e.target.value })}
                    onKeyDown={(e) => {
                        if (e.keyCode === 13) {
                            goToHash(loc)
                        }
                    }} />
                <i className='search link icon' onClick={() => goToHash(loc)}></i>
            </div>
        </div>
    }

    private renderLoggedInRightMenu() {
        const user = this.props.loggedInUser
        if (!user) {
            throw new Error('invalid user state')
        }
        return <div className='right menu'>
            <div className='ui dropdown item' ref={(x) => $(x!).dropdown({ action: () => $(x!).dropdown('hide') })}>
                <i className='list layout icon'></i> Decks
                <div className='menu'>
                    <a className='item' href='#/'>
                        <i className='list layout icon'></i> All Decks
                    </a>
                    <a className='item' href={`#/user/${encodeURIComponent(user)}`}>
                        <i className='user icon'></i> My Decks
                    </a>
                    <a className='item' onClick={() => this.showMf(this.newDeckMf)}>
                        <i className='add icon'></i> New Deck
                    </a>
                </div>
            </div>
            <div className='ui dropdown item' ref={(x) => $(x!).dropdown({ action: () => $(x!).dropdown('hide') })}>
                <i className='user icon'></i> {this.props.loggedInUser}
                <div className='menu'>
                    <a className='item' onClick={() => this.props.logout()}>
                        <i className='sign out icon'></i> Sign Out
                    </a>
                    <a className='item' onClick={() => this.showMf(this.changePasswordMf)}>
                        <i className='setting icon'></i> Change Password
                    </a>
                    <a className='item' onClick={() => this.showMf(this.deleteUserMf)}>
                        <i className='red ban icon'></i> Delete Account
                    </a>
                </div>
            </div>
        </div>
    }

    private renderNotLoggedInRightMenu() {
        return <div className='right menu'>
            <a className='item' onClick={() => this.showMf(this.createUserMf)}>
                Create Account
            </a>
            <a className='item' onClick={() => this.showMf(this.loginMf)}>
                <i className='user icon'></i> Sign In
            </a>
        </div>
    }

    private renderLoginModal() {
        return <div className='ui small modal' ref={(x) => this.loginMf.modal = x!}>
            <div className='header'>Sign In</div>
            <div className='content'>
                <div className='ui form' ref={(x) => this.loginMf.form = x!}>
                    <div className='field'>
                        <label>Username</label>
                        <div className='ui left icon input'>
                            <input
                                name='username'
                                type='text'
                                placeholder='Username'
                                value={this.state.editUser}
                                onChange={(e) => this.setState({ editUser: e.target.value })} />
                            <i className='user icon'></i>
                        </div>
                    </div>
                    <div className='field'>
                        <label>Password</label>
                        <div className='ui left icon input'>
                            <input
                                name='password'
                                type='password'
                                value={this.state.password}
                                onChange={(e) => this.setState({ password: e.target.value })} />
                            <i className='lock icon'></i>
                        </div>
                    </div>
                </div>
            </div>
            <div className='actions'>
                <input type='submit' name='submit' value='Sign In' className='ui primary ok button' />
                <div className='ui cancel button'>Cancel</div>
            </div>
        </div>
    }

    private renderUserModal() {
        return <div className='ui small modal' ref={(x) => this.createUserMf.modal = x!}>
            <div className='header'>Create Account</div>
            <div className='content'>
                <div className='ui form' ref={(x) => this.createUserMf.form = x!}>
                    <div className='field'>
                        <label>Username</label>
                        <div className='ui left icon input'>
                            <input
                                name='username'
                                type='text'
                                placeholder='Username'
                                value={this.state.editUser}
                                onChange={(e) => this.setState({ editUser: e.target.value })} />
                            <i className='user icon'></i>
                        </div>
                    </div>
                    <div className='field'>
                        <label>Password</label>
                        <div className='ui left icon input'>
                            <input
                                name='password'
                                type='password'
                                value={this.state.password}
                                onChange={(e) => this.setState({ password: e.target.value })} />
                            <i className='lock icon'></i>
                        </div>
                    </div>
                    <div className='field'>
                        <label>Confirm Password</label>
                        <div className='ui left icon input'>
                            <input name='passconfirm' type='password' />
                            <i className='lock icon'></i>
                        </div>
                    </div>
                </div>
            </div>
            <div className='actions'>
                <input type='submit' name='submit' value='Create Account' className='ui primary ok button' />
                <div className='ui cancel button'>Cancel</div>
            </div>
        </div>
    }

    private renderChangePasswordModal() {
        return <div className='ui small modal' ref={(x) => this.changePasswordMf.modal = x!}>
            <div className='header'>Change Password</div>
            <div className='content'>
                <div className='ui form' ref={(x) => this.changePasswordMf.form = x!}>
                    <div className='field'>
                        <label>New Password</label>
                        <div className='ui left icon input'>
                            <input
                                name='password'
                                type='password'
                                value={this.state.password}
                                onChange={(e) => this.setState({ password: e.target.value })} />
                            <i className='lock icon'></i>
                        </div>
                    </div>
                    <div className='field'>
                        <label>Confirm New Password</label>
                        <div className='ui left icon input'>
                            <input name='passconfirm' type='password' />
                            <i className='lock icon'></i>
                        </div>
                    </div>
                </div>
            </div>
            <div className='actions'>
                <input type='submit' name='submit' value='Update Account' className='ui primary ok button' />
                <div className='ui cancel button'>Cancel</div>
            </div>
        </div>
    }

    private renderDeleteUserModal() {
        return <div className='ui small modal' ref={(x) => this.deleteUserMf.modal = x!}>
            <div className='header'>Are You Sure?</div>
            <div className='content'>
                <h3 className='ui header'>
                    <i className='red warning icon'></i>
                    <div className='content'>
                        Warning! This will delete all of {this.props.loggedInUser}'s decks and account information.
                        <br />
                        Are you sure you want to do this?
                    </div>
                </h3>
                <div className='ui form' ref={(x) => this.deleteUserMf.form = x!}>
                    <div className='field'>
                        <label>Confirm Password</label>
                        <div className='ui left icon input'>
                            <input
                                name='password'
                                type='password'
                                value={this.state.password}
                                onChange={(e) => this.setState({ password: e.target.value })} />
                            <i className='lock icon'></i>
                        </div>
                    </div>
                </div>
            </div>
            <div className='actions'>
                <input
                    type='submit'
                    name='submit'
                    value={`Delete ${this.props.loggedInUser}`}
                    className='ui red ok button' />
                <div className='ui cancel button'>Cancel</div>
            </div>
        </div>
    }

    private renderNewDeckModal() {
        return <div className='ui small modal' ref={(x) => this.newDeckMf.modal = x!}>
            <div className='header'>Create Deck</div>
            <div className='content'>
                <div className='ui form' ref={(x) => this.newDeckMf.form = x!}>
                    <div className='field'>
                        <label>Deck Name</label>
                        <div className='ui input'>
                            <input
                                name='name'
                                type='text'
                                value={this.state.deckName}
                                onChange={(e) => this.setState({ deckName: e.target.value })} />
                        </div>
                    </div>
                    <div className='field'>
                        <div className='ui checkbox' ref={(x) => {
                            $(x!).checkbox({
                                onChange: () => this.setState({ deckIsPrivate: !this.state.deckIsPrivate }),
                            })
                        }}>
                            <input
                                type='checkbox'
                                tabIndex={0}
                                className='hidden'
                                checked={this.state.deckIsPrivate}
                                // Note: The onChange handler is here just to make react happy.
                                // The real work happens in the semantic UI checkbox change handler.
                                onChange={() => { return }} />
                            <label>This deck is private</label>
                        </div>
                    </div>
                </div>
            </div>
            <div className='actions'>
                <input type='submit' name='submit' value='Create Deck' className='ui primary ok button' />
                <div className='ui cancel button'>Cancel</div>
            </div>
        </div>
    }

    private setupMf(mf: ModalForm, action: () => void, fields: {}) {
        if (!mf.form || !mf.modal) {
            throw new Error('MF references not set correctly')
        }
        const [form, modal] = [mf.form, mf.modal]
        $(form).form({
            inline: true,
            on: 'submit',
            onSuccess: () => {
                $(modal).modal('hide')
                action()
            },
            fields,
        })
    }

    private showMf(mf: ModalForm) {
        if (!mf.form || !mf.modal) {
            throw new Error('MF references not set correctly')
        }
        const [form, modal] = [mf.form, mf.modal]
        $(modal)
            .modal({
                onApprove: (): false | void => {
                    $(form).form('validate form')
                    return false // don't close the modal until the form is submitted successfully
                },
            }).modal('show')
    }

    private login() {
        const input: dt.UserLoginInput = {
            name: this.state.editUser,
            password: this.state.password,
        }
        this.props.startLoading('Signing In...')
        $.post(`${dtApiRoot}/login`, JSON.stringify(input)).done((output: dt.UserLoginOutput) => {
            this.setState({ editUser: '', password: '' })
            this.props.login(input.name, output.token)
        }).fail((xhr: any) => {
            this.setState({ editUser: '', password: '' })
            this.props.showError(xhr.responseText)
        })
    }

    private createUser() {
        const input: dt.UserCreateInput = {
            name: this.state.editUser,
            password: this.state.password,
        }
        this.props.startLoading('Creating User...')
        $.post(`${dtApiRoot}/user`, JSON.stringify(input)).done((output: dt.UserCreateOutput) => {
            this.setState({ editUser: '', password: '' })
            this.props.login(input.name, output.token)
        }).fail((xhr: any) => {
            this.setState({ editUser: '', password: '' })
            this.props.showError(xhr.responseText)
        })
    }

    private deleteUser() {
        if (!this.props.loggedInUser || !this.props.apiToken) {
            return // should never happen
        }
        const input: dt.UserDeleteInput = {
            name: this.props.loggedInUser,
            token: this.props.apiToken,
            password: this.state.password,
        }
        this.props.startLoading('Deleting User...')
        $.ajax(`${dtApiRoot}/user`, {
            type: 'DELETE',
            data: JSON.stringify(input),
        }).done(() => {
            this.props.logout()
        }).fail((xhr: any) => {
            this.props.logout()
            this.props.showError(xhr.responseText)
        })
    }

    private changePassword() {
        if (!this.props.loggedInUser || !this.props.apiToken) {
            return // should never happen
        }
        const input: dt.UserUpdateInput = {
            name: this.props.loggedInUser,
            token: this.props.apiToken,
            password: this.state.password,
        }
        this.props.startLoading('Changing Password...')
        $.ajax(`${dtApiRoot}/user`, {
            type: 'PUT',
            data: JSON.stringify(input),
        }).done(() => {
            this.setState({ password: '' })
            this.props.stopLoading()
        }).fail((xhr: any) => {
            this.setState({ password: '' })
            this.props.logout()
            this.props.showError(xhr.responseText)
        })
    }

    private createNewDeck() {
        if (!this.props.loggedInUser || !this.props.apiToken) {
            return // should never happen
        }
        const [user, token] = [this.props.loggedInUser, this.props.apiToken]
        const deck = {
            name: this.state.deckName,
            isPrivate: this.state.deckIsPrivate,
            lastScanPrice: 0,
            lastScanDate: Date.now(),
            cards: [],
            sideboard: [],
            commander: undefined,
            notes: '',
        }
        const updateIn: dt.UserUpdateInput = { name: user, token, addDecks: [deck] }
        this.props.startLoading('Creating Deck...')
        $.ajax(`${dtApiRoot}/user`, {
            type: 'PUT',
            data: JSON.stringify(updateIn),
        }).done(() => {
            this.setState({ deckName: '', deckIsPrivate: false })
            goToHash(`/edit/${encodeURIComponent(user)}/${encodeURIComponent(deck.name)}`)
        }).fail((xhr: any) => {
            this.props.showError(xhr.responseText)
        })
    }
}
