import * as $ from 'jquery'
import * as React from 'react'
import * as dt from '../../common/model'
import { dtApiRoot, goToHash } from '../common'
import { DeckEditorComponent } from './deck-editor'
import { DeckListComponent } from './deck-list'
import { MenuComponent } from './menu'

interface IndexState {
    error: string | undefined
    loggedInUser: string | undefined
    apiToken: dt.LoginToken | undefined
    decks: dt.DeckWithUser[]
    editDeck: dt.DeckWithUser | undefined
    loadingText: string | undefined
}

export class IndexComponent extends React.Component<{}, IndexState> {
    private errorModal: HTMLElement
    private hashChangeListener: () => void

    constructor() {
        super({})

        const state: IndexState = {
            error: undefined,
            apiToken: undefined,
            loggedInUser: undefined,
            decks: [],
            editDeck: undefined,
            loadingText: undefined,
        }

        const savedUser = localStorage.getItem('user')
        const savedToken = localStorage.getItem('token')
        if (savedUser && savedToken) {
            state.loggedInUser = savedUser
            state.apiToken = savedToken
        }

        this.state = state
    }

    public componentDidMount() {
        this.followHashLocation()
        this.hashChangeListener = () => this.followHashLocation()
        window.addEventListener('hashchange', this.hashChangeListener, false)
    }

    public componentWillUnmount() {
        window.removeEventListener('hashchange', this.hashChangeListener, false)
    }

    public render() {
        return <div className='ui page grid'>
            {this.renderBanner()}
            <MenuComponent
                loggedInUser={this.state.loggedInUser}
                apiToken={this.state.apiToken}
                login={(user, token) => this.login(user, token)}
                logout={() => this.logout()}
                startLoading={(x) => this.setState({ loadingText: x })}
                stopLoading={() => this.setState({ loadingText: undefined })}
                showError={(err) => this.showError(err)} />
            {this.state.editDeck &&
                <DeckEditorComponent
                    loggedInUser={this.state.loggedInUser}
                    apiToken={this.state.apiToken}
                    deck={this.state.editDeck}
                    startLoading={(x) => this.setState({ loadingText: x })}
                    showError={(err) => this.showError(err)} />}
            {!this.state.editDeck &&
                <DeckListComponent
                    showError={(err) => this.showError(err)}
                    decks={this.state.decks} />}
            <div className={`ui ${this.state.loadingText && 'active'} inverted page dimmer`}>
                <div className='ui active massive text loader'>{this.state.loadingText}</div>
            </div>
            {this.renderErrorModal()}
        </div>
    }

    private renderBanner() {
        return <div className='row'>
            <div className='column'>
                <h2 className='ui center aligned banner header'>
                    <a href='#/'>donkey townsfolk</a>
                </h2>
                <h3 className='ui center aligned sub-banner header'>price limited deck tracker</h3>
            </div>
        </div>
    }

    private renderErrorModal() {
        return <div className='ui small modal' ref={(x) => this.errorModal = x!}>
            <div className='header'>An error has occurred!</div>
            <div className='content'>
                <h3 className='ui header'>
                    <i className='red warning circle icon'></i>
                    <div className='content'>
                        {this.state.error}
                    </div>
                </h3>
            </div>
            <div className='actions'>
                <div className='ui primary ok button'>Close</div>
            </div>
        </div>
    }

    private followHashLocation() {
        // Removes the '#', and any leading/final '/' characters
        const loc = window.location.hash.replace(/^#\/?|\/$/g, '').split('/').map(decodeURIComponent)
        switch (loc[0].toLowerCase()) {
            default:
            case '':
                this.queryDecks({})
                break
            case 'search':
                if (loc.length >= 2) {
                    this.queryDecks({ filterQuery: loc[1] })
                } else {
                    this.queryDecks({})
                }
                break
            case 'user':
                if (loc.length >= 2) {
                    this.queryDecks({ filterUser: loc[1] })
                } else {
                    this.showError('incorrect hash location')
                }
                break
            case 'edit':
                if (loc.length >= 3) {
                    this.editDeck(loc[1], loc[2])
                } else {
                    this.showError('incorrect hash location')
                }
                break
        }
    }

    private showError(text: string) {
        text = text ? text : 'unknown error'
        this.setState({ error: this.formatError(text), loadingText: undefined }, () => {
            if (!this.errorModal) {
                throw new Error('Error modal reference not set correctly')
            }
            $(this.errorModal).modal('show')
        })
    }

    private login(user: string, token: dt.LoginToken) {
        localStorage.setItem('user', user)
        localStorage.setItem('token', token)
        this.setState({ loggedInUser: user, apiToken: token }, () => goToHash(`/user/${encodeURIComponent(user)}`))
    }

    private logout() {
        localStorage.removeItem('user')
        localStorage.removeItem('token')
        this.setState({ loggedInUser: undefined, apiToken: undefined }, () => goToHash('/'))
    }

    private formatError(s: string) {
        s = s.trim()
        if (s.length > 0) {
            s = s.charAt(0).toUpperCase() + s.slice(1)
        }
        if (!s.endsWith('.')) {
            s += '.'
        }
        return s
    }

    private queryDecks(data: { filterQuery?: string, filterUser?: string }) {
        this.setState({ loadingText: 'Loading Decks...' })
        const input: dt.DecksQueryInput = {}
        input.filterQuery = data.filterQuery
        input.filterUser = data.filterUser
        if (this.state.loggedInUser && this.state.apiToken) {
            input.loggedInUser = {  name: this.state.loggedInUser, token: this.state.apiToken }
        }
        $.post(`${dtApiRoot}/decks`, JSON.stringify(input)).done((output: dt.DecksQueryOutput) => {
            this.setState({ decks: output.decks, editDeck: undefined, loadingText: undefined })
        }).fail((xhr: any) => {
            this.showError(xhr.responseText)
        })
    }

    private editDeck(userName: string, deckName: string) {
        this.setState({ loadingText: 'Loading Deck...' })
        const input: dt.DecksQueryInput = {}
        input.filterUser = userName
        if (this.state.loggedInUser && this.state.apiToken) {
            input.loggedInUser = {  name: this.state.loggedInUser, token: this.state.apiToken }
        }
        $.post(`${dtApiRoot}/decks`, JSON.stringify(input)).done((output: dt.DecksQueryOutput) => {
            const deck = output.decks.find((x) => x.deck.name === deckName)
            if (deck) {
                this.setState({ decks: [], editDeck: deck, loadingText: undefined })
            } else {
                this.showError('deck not found')
                this.queryDecks({})
            }
        }).fail((xhr: any) => {
            this.showError(xhr.responseText)
        })
    }
}
