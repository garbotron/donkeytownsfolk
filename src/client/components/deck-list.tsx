import * as React from 'react'
import * as dt from '../../common/model'

export interface DeckListProps {
    decks: dt.DeckWithUser[]
    showError: (text: string) => void
}

interface DeckListState {
    startIdx: number
}

const maxDecksPerPage = 25

export class DeckListComponent extends React.Component<DeckListProps, DeckListState> {
    constructor(props: DeckListProps) {
        super(props)
        this.state = { startIdx: 0 }
    }

    public render() {
        return <div className='row'>
            <div className='column'>
                <table className='ui unstackable compact blue table'>
                    <thead>
                        <tr>
                            <th>Deck</th>
                            <th>User</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.decks
                            .slice(this.state.startIdx, this.state.startIdx + maxDecksPerPage)
                            .map((x) => this.renderDeckRow(x))}
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colSpan={3}>{this.renderPaginationMenu()}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    }

    private renderDeckRow(deck: dt.DeckWithUser) {
        const formatter = new Intl.NumberFormat('en-US', {
            currency: 'USD',
            minimumFractionDigits: 2,
            style: 'currency',
        })
        const date = new Date(deck.deck.lastScanDate)
        const userName = encodeURIComponent(deck.user)
        const deckName = encodeURIComponent(deck.deck.name)
        const url = `#/edit/${userName}/${deckName}`
        return <tr key={`${userName}-${deckName}`}>
            <td>
                <strong>
                    <a href={url} title='Click to view/edit this deck.'>{deck.deck.name}</a>
                </strong>
                &nbsp;
                {deck.deck.isPrivate && <i className='grey privacy icon' title='This deck is private.'></i>}
            </td>
            <td>{deck.user}</td>
            <td>
                {formatter.format(deck.deck.lastScanPrice)}
                <span className='scan-date'>(as of {date.toLocaleDateString()})</span>
            </td>
        </tr>
    }

    private renderPaginationMenu() {
        const numPages = Math.ceil(this.props.decks.length / maxDecksPerPage)
        let curPage = Math.floor(this.state.startIdx / maxDecksPerPage)
        if (curPage >= numPages) {
            curPage = 0
        }

        const menuItems: JSX.Element[] = []

        if (curPage === 0) {
            menuItems.push(<a className='disabled icon item'><i className='left chevron icon'></i></a>)
        } else {
            const action = () => this.setState({ startIdx: (curPage - 1) * maxDecksPerPage })
            menuItems.push(<a className='icon item' onClick={action}><i className='left chevron icon'></i></a>)
        }

        const pagesToRender = [0, curPage - 1, curPage, curPage + 1, numPages - 1]
            .filter((x) => x >= 0 && x < numPages)
            .filter((v, i, a) => a.indexOf(v) === i) // unique elements

        let lastPage = -1
        for (const page of pagesToRender) {
            if (lastPage >= 0 && (page - lastPage) > 1) {
                menuItems.push(<a className='disabled item'>...</a>)
            }
            const className = page === curPage ? 'active item' : 'item'
            const action = () => this.setState({ startIdx: page * maxDecksPerPage })
            menuItems.push(<a className={className} onClick={action}>{page}</a>)
            lastPage = page
        }

        if (curPage >= numPages - 1) {
            menuItems.push(<a className='disabled icon item'><i className='right chevron icon'></i></a>)
        } else {
            const action = () => this.setState({ startIdx: (curPage + 1) * maxDecksPerPage })
            menuItems.push(<a className='icon item' onClick={action}><i className='right chevron icon'></i></a>)
        }

        return <div className='ui right floated small borderless pagination menu'>
            {menuItems}
        </div>
    }
}
