import * as $ from 'jquery'
import * as React from 'react'
import * as dt from '../../common/model'
import { dtApiRoot, goToHash } from '../common'

export interface DeckEditorProps {
    loggedInUser: string | undefined
    apiToken: dt.LoginToken | undefined
    deck: dt.DeckWithUser
    startLoading: (text: string) => void
    showError: (text: string) => void
}

interface DeckEditorState {
    name: string
    isPrivate: boolean
    decklist: string
    commander: string
    sideboard: string
    notes: string
}

interface ModalForm {
    modal: HTMLElement | undefined
    form: HTMLElement | undefined
}

function tablesort(elem: JQuery) {
    (elem as any).tablesort() // we don't have proper typescript support for this
}

export class DeckEditorComponent extends React.Component<DeckEditorProps, DeckEditorState> {
    private imgPreviewMain: HTMLElement
    private imgPreviewSideboard: HTMLElement
    private deleteDeckModal: HTMLElement
    private deckInfoMf: ModalForm = { modal: undefined, form: undefined }
    private decklistMf: ModalForm = { modal: undefined, form: undefined }

    constructor(props: DeckEditorProps) {
        super(props)
        this.state = { name: '', isPrivate: false, decklist: '', commander: '', sideboard: '', notes: '' }
    }

    public componentDidMount() {
        this.setState({
            name: this.props.deck.deck.name,
            isPrivate: this.props.deck.deck.isPrivate,
            decklist: this.decklistToDump(this.props.deck.deck.cards),
            commander: this.props.deck.deck.commander ? this.props.deck.deck.commander.name : '',
            sideboard: this.decklistToDump(this.props.deck.deck.sideboard),
            notes: this.props.deck.deck.notes,
        })

        this.setupMf(this.deckInfoMf, () => this.updateDeckInfo(), {
            name: {
                identifier: 'name',
                rules: [{
                    type: 'empty',
                    prompt: 'Please enter a deck name',
                }],
            },
        })

        this.setupMf(this.decklistMf, () => this.updateDecklist(), {})
    }

    public render() {
        const date = new Date(this.props.deck.deck.lastScanDate)
        return <div className='row'>
            <div className='column'>
                <h1 className='ui top attached header'>
                    {this.props.deck.deck.name}
                    {this.hasEditRights() && [
                        <a className='ui red right floated compact button' onClick={() => this.showDeleteDeckModal()}>
                            <i className='delete icon'></i>
                            Delete Deck
                        </a>,
                        <a className='ui right floated compact button' onClick={() => this.showMf(this.deckInfoMf)}>
                            <i className='settings icon'></i>
                            Change Deck Info
                        </a>,
                    ]}
                    <div className='sub header'>
                        Created by {this.props.deck.user}
                        {this.props.deck.deck.isPrivate && <span className='private'>(private)</span>}
                    </div>
                </h1>
                {this.props.deck.deck.notes && <div className='ui attached segment'>
                    <h4 className='ui header' style={{ whiteSpace: 'pre-wrap' }}>{this.props.deck.deck.notes}</h4>
                </div>}
                <div className='ui bottom attached segment'>
                    <h3 className='ui header'>
                        Current price: {this.formatPrice(this.props.deck.deck.lastScanPrice)}
                        <span className='scan-date'>(last scanned {date.toLocaleString()})</span>
                    </h3>
                </div>
                <h2 className='ui top attached header'>
                    Decklist ({this.props.deck.deck.cards.reduce((s, c) => s + c.count, 0)} cards)
                    {this.hasEditRights() && [
                        <a className='ui right floated compact button' onClick={() => this.rescanPrices()}>
                            <i className='dollar icon'></i>
                            Rescan Prices
                        </a>,
                        <a className='ui right floated compact button' onClick={() => this.showMf(this.decklistMf)}>
                            <i className='edit icon'></i>
                            Update Decklist
                        </a>,
                    ]}
                </h2>
                {this.renderDecklist()}
                {this.hasSideboard() && [
                    <h2 className='ui top attached header'>
                        Sideboard ({this.props.deck.deck.sideboard.reduce((s, c) => s + c.count, 0)} cards)
                    </h2>,
                    this.renderSideboard(),
                ]}
                {this.renderDeleteDeckModal()}
                {this.renderDeckInfoModal()}
                {this.renderDecklistModal()}
            </div>
        </div>
    }

    private renderDecklist() {
        return <div className='ui bottom attached segment'>
            <div className='ui equal height grid'>
                <div className='row'>
                    <div className='eleven wide column'>
                        <table className='ui very compact selectable sortable deck table' ref={(x) => tablesort($(x!))}>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th className='collapsing'>Price Each</th>
                                    <th className='collapsing'>Total Price</th>
                                </tr>
                                {this.props.deck.deck.commander &&
                                    this.renderDecklistCommander(this.props.deck.deck.commander)}
                            </thead>
                            <tbody>
                                {this.props.deck.deck.cards.map(
                                    (x) => this.renderDecklistEntry(x, () => this.imgPreviewMain))}
                            </tbody>
                        </table>
                    </div>
                    <div className='five wide column'>
                        <img
                            ref={(x) => this.imgPreviewMain = x!}
                            className='ui centered image'
                            style={{ width: '200px' }}
                            src='http://gatherer.wizards.com/Handlers/Image.ashx?type=card&amp;name=invalid' />
                    </div>
                </div>
            </div>
        </div>
    }

    private renderSideboard() {
        return <div className='ui bottom attached segment'>
            <div className='ui equal height grid'>
                <div className='row'>
                    <div className='eleven wide column'>
                        <table className='ui very compact selectable sortable deck table' ref={(x) => tablesort($(x!))}>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th className='collapsing'>Price Each</th>
                                    <th className='collapsing'>Total Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.props.deck.deck.sideboard.map(
                                    (x) => this.renderDecklistEntry(x, () => this.imgPreviewSideboard))}
                            </tbody>
                        </table>
                    </div>
                    <div className='five wide column'>
                        <img
                            ref={(x) => this.imgPreviewSideboard = x!}
                            className='ui centered image'
                            style={{ width: '200px' }}
                            src='http://gatherer.wizards.com/Handlers/Image.ashx?type=card&amp;name=invalid' />
                    </div>
                </div>
            </div>
        </div>
    }

    private updateCardImageSrc(img: HTMLElement, card: dt.Card) {
        $(img).attr(
            'src',
            'http://gatherer.wizards.com/Handlers/Image.ashx?type=card&name=' + encodeURIComponent(card.name))
    }

    private renderDecklistEntry(card: dt.Card, getImgRef: () => HTMLElement) {
        const url = `http://magiccards.info/query?q=%21${card.name}&amp;v=card&amp;s=cname`
        const singlePrice = card.lastScanPrice === 'NOT_FOUND' ? 0 : card.lastScanPrice
        const totalPrice = singlePrice * card.count
        return <tr onMouseMove={() => this.updateCardImageSrc(getImgRef(), card)}>
            <td data-sort-value={card.count} className='collapsing'>{card.count}</td>
            <td data-sort-value={card.name}>
                <a href={url}>{card.name}</a>
                {card.lastScanPrice === 'NOT_FOUND' && this.renderNotFoundIcon()}
            </td>
            <td data-sort-value={singlePrice} className='collapsing'>{this.formatPrice(singlePrice)}</td>
            <td data-sort-value={totalPrice} className='collapsing'>{this.formatPrice(totalPrice)}</td>
        </tr>
    }

    private renderDecklistCommander(commander: dt.Card) {
        const url = `http://magiccards.info/query?q=%21${commander.name}&amp;v=card&amp;s=cname`
        return <tr onMouseMove={() => this.updateCardImageSrc(this.imgPreviewMain, commander)}>
            <td></td>
            <td>
                <strong>
                    Commander: <a href={url}>{commander.name}</a>
                    {commander.lastScanPrice === 'NOT_FOUND' && this.renderNotFoundIcon()}
                </strong>
            </td>
            <td className='collapsing'>{this.formatPrice(commander.lastScanPrice)}</td>
            <td className='collapsing'>{this.formatPrice(commander.lastScanPrice)}</td>
        </tr>
    }

    private renderNotFoundIcon() {
        return <span className='ui red horizontal label' style={{ marginLeft: '1em' }}>
            <i className='warning sign icon'></i> NOT FOUND
        </span>
    }

    private renderDeleteDeckModal() {
        return <div className='ui small modal' ref={(x) => this.deleteDeckModal = x!}>
            <div className='header'>Are You Sure?</div>
            <div className='content'>
                <h3 className='ui header'>
                    <i className='red warning icon'></i>
                    <div className='content'>
                        Warning! This can't be undone.
                        <br />
                        Are you sure you want to do this?
                    </div>
                </h3>
            </div>
            <div className='actions'>
                <div className='ui red ok button'>{`Delete ${this.props.deck.deck.name}`}</div>
                <div className='ui cancel button'>Cancel</div>
            </div>
        </div>
    }

    private renderDeckInfoModal() {
        return <div className='ui small modal' ref={(x) => this.deckInfoMf.modal = x!}>
            <div className='header'>Edit Deck Info</div>
            <div className='content'>
                <div className='ui form' ref={(x) => this.deckInfoMf.form = x!}>
                    <div className='field'>
                        <label>Deck Name</label>
                        <div className='ui input'>
                            <input
                                type='text'
                                value={this.state.name}
                                onChange={(e) => this.setState({ name: e.target.value })} />
                        </div>
                    </div>
                    <div className='field'>
                        <div className='ui checkbox' ref={(x) => {
                            $(x!).checkbox({
                                onChange: () => this.setState({ isPrivate: !this.state.isPrivate }),
                            })
                        }}>
                            <input
                                type='checkbox'
                                tabIndex={0}
                                className='hidden'
                                checked={this.state.isPrivate}
                                // Note: The onChange handler is here just to make react happy.
                                // The real work happens in the semantic UI checkbox change handler.
                                onChange={() => { return }} />
                            <label>This deck is private</label>
                        </div>
                    </div>
                    <div className='field'>
                        <label>Notes</label>
                        <textarea
                            placeholder='General notes about this deck...'
                            value={this.state.notes}
                            onChange={(e) => this.setState({ notes: e.target.value })} />
                    </div>
                </div>
            </div>
            <div className='actions'>
                <input type='submit'value='Update Deck' className='ui primary ok button' />
                <div className='ui cancel button'>Cancel</div>
            </div>
        </div>
    }

    private renderDecklistModal() {
        return <div className='ui small modal' ref={(x) => this.decklistMf.modal = x!}>
            <div className='header'>Edit Decklist</div>
            <div className='content'>
                <div className='ui form' ref={(x) => this.decklistMf.form = x!}>
                    <div className='field'>
                        <label>Commander (blank for none)</label>
                        <div className='ui input'>
                            <input
                                type='text'
                                value={this.state.commander}
                                onChange={(e) => this.setState({ commander: e.target.value })} />
                        </div>
                    </div>
                    <div className='two fields'>
                        <div className='field'>
                            <label>Decklist</label>
                            <textarea
                                placeholder='4 Black Lotus\n4 Ancestral Recall\n1 Cheap Ass'
                                value={this.state.decklist}
                                onChange={(e) => this.setState({ decklist: e.target.value })} />
                        </div>
                        <div className='field'>
                            <label>Sideboard</label>
                            <textarea
                                value={this.state.sideboard}
                                onChange={(e) => this.setState({ sideboard: e.target.value })}/>
                        </div>
                    </div>
                </div>
            </div>
            <div className='actions'>
                <input type='submit' value='Update Deck' className='ui primary ok button' />
                <div className='ui cancel button'>Cancel</div>
            </div>
        </div>
    }

    private setupMf(mf: ModalForm, action: () => void, fields: {}) {
        if (!mf.form || !mf.modal) {
            throw new Error('MF references not set correctly')
        }
        const [form, modal] = [mf.form, mf.modal]
        $(form).form({
            inline: true,
            on: 'submit',
            onSuccess: () => {
                $(modal).modal('hide')
                action()
            },
            fields,
        })
    }

    private showMf(mf: ModalForm) {
        if (!mf.form || !mf.modal) {
            throw new Error('MF references not set correctly')
        }
        const [form, modal] = [mf.form, mf.modal]
        $(modal)
            .modal({
                onApprove: (): false | void => {
                    $(form).form('validate form')
                    return false // don't close the modal until the form is submitted successfully
                },
            })
            .modal('show')
    }

    private showDeleteDeckModal() {
        $(this.deleteDeckModal)
            .modal({ onApprove: (): false | void => this.deleteDeck() })
            .modal('show')
    }

    private formatPrice(price: number | 'NOT_FOUND') {
        price = (price === 'NOT_FOUND') ? 0 : price
        const formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2,
        })
        return formatter.format(price)
    }

    private decklistToDump(cards: dt.Card[]): string {
        const lines: string[] = []
        for (const card of cards) {
            lines.push(`${card.count} ${card.name}`)
        }
        return lines.join('\n')
    }

    private dumpToDecklist(dump: string): dt.Card[] {
        const cards: dt.Card[] = []
        for (const line of dump.split(/\r?\n/)) {
            const match = line.trim().match(/^([0-9]*)x?\s+(.*)$/)
            if (match) {
                const count = match[1] ? +match[1] : 1
                cards.push({ count, name: match[2], lastScanPrice: 'NOT_FOUND' })
            }
        }
        return cards
    }

    private hasSideboard(): boolean {
        return (this.props.deck.deck.sideboard.length > 0)
    }

    private hasEditRights(): boolean {
        return (this.props.loggedInUser === this.props.deck.user)
    }

    private cloneDeck() {
        return jQuery.extend(true, {}, this.props.deck.deck) as dt.Deck // clone deck
    }

    private updateDeckInfo() {
        if (!this.props.loggedInUser || !this.props.apiToken) {
            return // should never happen
        }
        const [user, token] = [this.props.loggedInUser, this.props.apiToken]
        const deck = this.cloneDeck()
        deck.name = this.state.name
        deck.isPrivate = this.state.isPrivate
        deck.notes = this.state.notes
        const input: dt.UserUpdateInput = {
            name: user,
            token,
            removeDeckNames: [this.props.deck.deck.name],
            addDecks: [deck],
        }
        this.props.startLoading('Updating Deck...')
        $.ajax(`${dtApiRoot}/user`, {
            type: 'PUT',
            data: JSON.stringify(input),
        }).done(() => {
            goToHash(`/edit/${encodeURIComponent(user)}/${encodeURIComponent(deck.name)}`)
        }).fail((xhr: any) => {
            this.props.showError(xhr.responseText)
        })
    }

    private deleteDeck() {
        if (!this.props.loggedInUser || !this.props.apiToken) {
            return // should never happen
        }
        const [user, token] = [this.props.loggedInUser, this.props.apiToken]
        const input: dt.UserUpdateInput = {
            name: user,
            token,
            removeDeckNames: [this.props.deck.deck.name],
        }
        this.props.startLoading('Deleting Deck...')
        $.ajax(`${dtApiRoot}/user`, {
            type: 'PUT',
            data: JSON.stringify(input),
        }).done(() => {
            goToHash('/')
        }).fail((xhr: any) => {
            this.props.showError(xhr.responseText)
        })
    }

    private rescanPrices() {
        if (!this.props.loggedInUser || !this.props.apiToken) {
            return // should never happen
        }
        const [user, token] = [this.props.loggedInUser, this.props.apiToken]
        const deck = this.cloneDeck()
        this.props.startLoading('Rescanning Prices...')
        this.rescan(deck, () => {
            const input: dt.UserUpdateInput = {
                name: user,
                token,
                removeDeckNames: [this.props.deck.deck.name],
                addDecks: [deck],
            }
            $.ajax(`${dtApiRoot}/user`, {
                type: 'PUT',
                data: JSON.stringify(input),
            }).done(() => {
                goToHash(`/edit/${encodeURIComponent(user)}/${encodeURIComponent(deck.name)}`)
            }).fail((xhr: any) => {
                this.props.showError(xhr.responseText)
            })
        })
    }

    private updateDecklist() {
        if (!this.props.loggedInUser || !this.props.apiToken) {
            return // should never happen
        }
        const [user, token] = [this.props.loggedInUser, this.props.apiToken]
        const deck = this.cloneDeck()
        deck.cards = this.dumpToDecklist(this.state.decklist)
        deck.sideboard = this.dumpToDecklist(this.state.sideboard)
        deck.commander = this.state.commander
            ? { name: this.state.commander.trim(), count: 1, lastScanPrice: 'NOT_FOUND' }
            : undefined
        this.props.startLoading('Updating Decklist / Scanning Prices...')
        this.rescan(deck, () => {
            const input: dt.UserUpdateInput = {
                name: user,
                token,
                removeDeckNames: [this.props.deck.deck.name],
                addDecks: [deck],
            }
            $.ajax(`${dtApiRoot}/user`, {
                type: 'PUT',
                data: JSON.stringify(input),
            }).done(() => {
                goToHash(`/edit/${encodeURIComponent(user)}/${encodeURIComponent(deck.name)}`)
            }).fail((xhr: any) => {
                this.props.showError(xhr.responseText)
            })
        })
    }

    private rescan(deck: dt.Deck, done: () => void) {
        let names = deck.cards.map((x) => x.name)
        if (deck.commander) {
            names.push(deck.commander.name)
        }
        names = names.concat(deck.sideboard.map((x) => x.name))

        const input: dt.PricesQueryInput = { cardKeys: names.map(dt.cardNameToKey) }
        $.post(`${dtApiRoot}/prices`, JSON.stringify(input)).done((output: dt.PricesQueryOutput) => {
            let idx = 0
            deck.lastScanPrice = 0
            function updateCard(card: dt.Card) {
                if (output.results[idx].result === dt.FindResult.Found) {
                    card.name = output.results[idx].name
                    card.lastScanPrice = output.results[idx].price
                    deck.lastScanPrice += card.lastScanPrice * card.count
                } else {
                    card.lastScanPrice = 'NOT_FOUND'
                }
                idx++
            }
            for (const card of deck.cards) {
                updateCard(card)
            }
            if (deck.commander) {
                updateCard(deck.commander)
            }
            for (const card of deck.sideboard) {
                updateCard(card)
            }
            deck.lastScanDate = Date.now()
            done()
        }).fail((xhr: any) => {
            this.props.showError(xhr.responseText)
        })
    }
}
