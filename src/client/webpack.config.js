const path = require('path');

module.exports = {
    entry:  "./src/client/index.tsx",
    output: {
        path: path.resolve(__dirname, "../../build/client"),
        filename: "client.js",
    },

    devtool: "source-map",

    resolve: {
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },

    module: {
        rules: [
            { test: /\.tsx?$/, loader: "ts-loader" },
            { enforce: 'pre', test: /\.tsx?$/, loader: 'tslint-loader', options: { emitErrors: true } },
            { enforce: 'pre', test: /\.js$/, loader: "source-map-loader" }
        ]
    },

    externals: {
        "jquery": "$",
        "react": "React",
        "react-dom": "ReactDOM"
    },
};
