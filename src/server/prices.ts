import * as request from 'request-promise-native'
import * as dt from '../common/model'
import * as server from './common'

const freeCards: { [key: string]: string } = {
    plains: 'Plains',
    island: 'Island',
    swamp: 'Swamp',
    mountain: 'Mountain',
    forest: 'Forest',
}

const dbCollection = process.env.DONKEYTOWNSFOLK_MONGODB_COLLECTION_PRICES!
if (!dbCollection) {
    throw new Error('DONKEYTOWNSFOLK_MONGODB_COLLECTION_PRICES not set')
}

export function pricesQuery(rawInput: server.RawInput, _: server.Context, callback: server.Callback) {
    return server.run('query prices', rawInput, callback, run)

    function run(input: dt.PricesQueryInput): Promise<dt.PricesQueryOutput> {
        return validateInput(input)
            .then(getCards)
            .then((cards) => ({ results: cards }))
    }

    function validateInput(input: dt.PricesQueryInput): Promise<dt.PricesQueryInput> {
        if (input.cardKeys === undefined) {
            return Promise.reject({ code: 400, message: '"cardKeys" parameter not specified' })
        } else {
            return Promise.resolve(input)
        }
    }

    function getCards(input: dt.PricesQueryInput): Promise<dt.CardPriceResult[]> {
        return Promise.all(input.cardKeys.map(getCard))
    }

    function getCard(name: string): Promise<dt.CardPriceResult> {
        const key = dt.cardNameToKey(name)
        if (key in freeCards) {
            return Promise.resolve({ result: dt.FindResult.Found, name: freeCards[key], price: 0 })
        }
        return server.db(dbCollection).findOne({ key }, { fields: { name: 1, price: 1 } })
            .then<dt.CardPriceDbEntry | null>((x) => x)
            .then((x) => x ?
                { result: dt.FindResult.Found, name: x.name, price: x.price } :
                { result: dt.FindResult.NotFound, name, price: 0 })
    }
}

function fetchJson<T>(uri: string): Promise<T> {
     // use request rather than default nodejs since it handles redirects, etc (and provides a promise API)
     return request.get(uri).then(JSON.parse)
}

export function pricesScrape(input: server.RawInput, _: server.Context, callback: server.Callback) {
    return server.run('scrape prices', input, callback, run)

    function run(__: dt.PricesScrapeInput): Promise<dt.PricesScrapeOutput> {
        return scrape()
            .then(deleteOldPrices)
            .then(addNewPrices)
            .then(() => ({}))
    }

    function scrape(): Promise<dt.CardPriceDbEntry[]> {
        const prices: dt.CardPriceDbEntry[] = []
        const limiter = new server.RateLimiter(2, 200)
        return scrapeSetList()
            .then((setNumbers) => {
                return Promise.all(setNumbers.map((setNumber) => {
                    return scrapeSetPage(limiter, setNumber).then((cards) => {
                        for (const card of cards) {
                            const entry = prices.find((x) => x.key === card.key)
                            if (entry) {
                                if (card.price < entry.price) {
                                    entry.price = card.price
                                }
                            } else {
                                prices.push(card)
                            }
                        }
                    })
                }))
            })
            .then(() => prices)
    }

    // scrape the main sets page - returns a set of set numbers that can be translated to set URIs
    function scrapeSetList(): Promise<number[]> {
        console.log('scraping set list...')
        return fetchJson<Array<{ id: number }>>('https://api.mtgstocks.com/card_sets')
            .then<number[]>((sets) => {
                if (sets.length === 0 || !sets[0].id) {
                    return Promise.reject({ code: 500, message: 'sets list document was malformed' })
                }
                return sets.map((x) => x.id)
            })
    }

    // scrapes all card prices from the given set page
    function scrapeSetPage(limiter: server.RateLimiter, setNumber: number): Promise<dt.CardPriceDbEntry[]> {
        const uri = `https://api.mtgstocks.com/card_sets/${setNumber}`
        interface SetInfo {
            prints: Array<{ name: string, latest_price?: number }>
        }
        return limiter.schedule(() => fetchJson<SetInfo>(uri))
            .then<dt.CardPriceDbEntry[]>((info) => {
                if (!info.prints || info.prints.length === 0 || !info.prints[0].name) {
                    return Promise.reject({ code: 500, message: `set info ${setNumber} was malformed` })
                }
                const cards = info.prints
                    .filter((x) => x.latest_price)
                    .map((x) => ({ name: x.name, price: x.latest_price as number, key: dt.cardNameToKey(x.name) }))
                console.log(`set #${setNumber} scraped: ${cards.length} cards found`)
                return cards
            })
    }

    function deleteOldPrices(toAdd: dt.CardPriceDbEntry[]): Promise<dt.CardPriceDbEntry[]> {
        console.log('deleting all cards')
        return server.db(dbCollection).deleteMany({}).then(() => toAdd)
    }

    function addNewPrices(cards: dt.CardPriceDbEntry[]): Promise<void> {
        console.log(`adding ${cards.length} cards`)
        return server.db(dbCollection).insertMany(cards).then(() => { return })
    }
}
