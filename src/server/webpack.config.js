const nodeExternals = require('webpack-node-externals');
const path = require('path');

module.exports = {
    entry:  {
        user: path.resolve(__dirname, './user.ts'),
        decks: path.resolve(__dirname, './decks.ts'),
        prices: path.resolve(__dirname, './prices.ts')
    },

    output: {
        libraryTarget: 'commonjs',
        path: path.resolve(__dirname, '../../build/server'),
        filename: '[name].js'
    },

    devtool: "source-map",

    resolve: {
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },

    module: {
        rules: [
            { test: /\.tsx?$/, loader: 'ts-loader' },
            { enforce: 'pre', test: /\.tsx?$/, loader: 'tslint-loader', options: { emitErrors: true } },
            { enforce: 'pre', test: /\.js$/, loader: 'source-map-loader' }
        ]
    },

    target: 'node',
    externals: [nodeExternals()]
};
