import * as bcrypt from 'bcryptjs'
import * as uuid from 'uuid'
import * as dt from '../common/model'
import * as server from './common'

const dbCollection = process.env.DONKEYTOWNSFOLK_MONGODB_COLLECTION_USERS!
if (!dbCollection) {
    throw new Error('DONKEYTOWNSFOLK_MONGODB_COLLECTION_USERS not set')
}

function generateLoginToken(): string {
    return uuid()
}

function findUser(name: string): Promise<dt.User> {
    return server.db(dbCollection).findOne({ name })
        .then<dt.User | null>((x) => x)
        .then((x) => x ? Promise.resolve(x) : Promise.reject({ code: 404, message: 'user not found' }))
}

export function userLogin(rawInput: server.RawInput, _: server.Context, callback: server.Callback) {
    return server.run('user login', rawInput, callback, run)

    function run(input: dt.UserLoginInput): Promise<dt.UserLoginOutput> {
        return validateInput(input)
            .then(lookupUser)
            .then(checkPassword)
    }

    function validateInput(input: dt.UserLoginInput): Promise<dt.UserLoginInput> {
        if (input.name === undefined) {
            return Promise.reject({ code: 400, message: '"name" parameter not specified' })
        } else if (input.password === undefined) {
            return Promise.reject({ code: 400, message: '"password" parameter not specified' })
        } else {
            return Promise.resolve(input)
        }
    }

    interface LookupUserRet { input: dt.UserLoginInput, user: dt.User }
    function lookupUser(input: dt.UserLoginInput): Promise<LookupUserRet> {
        return findUser(input.name).then((x) => ({ input, user: x }))
    }

    function checkPassword(data: LookupUserRet): Promise<dt.UserLoginOutput> {
        return bcrypt.compare(data.input.password, data.user.passwordHash).then((match) => {
            if (match) {
                const token = generateLoginToken()
                const tokenHash = bcrypt.hashSync(token, 10)
                const result: dt.UserLoginOutput = { token }
                return server.db(dbCollection).updateOne({ name: data.user.name }, { $set: { tokenHash } })
                    .then(() => result)
            } else {
                return Promise.reject<dt.UserLoginOutput>({ code: 403, message: 'password was incorrect' })
            }
        })
    }
}

export function userCreate(rawInput: server.RawInput, _: server.Context, callback: server.Callback) {
    return server.run('user create', rawInput, callback, run)

    function run(input: dt.UserCreateInput): Promise<dt.UserCreateOutput> {
        return validateInput(input)
            .then(lookupUser)
            .then(createUser)
    }

    function validateInput(input: dt.UserCreateInput): Promise<dt.UserCreateInput> {
        if (input.name === undefined) {
            return Promise.reject({ code: 400, message: '"name" parameter not specified' })
        } else if (input.password === undefined) {
            return Promise.reject({ code: 400, message: '"password" parameter not specified' })
        } else {
            return Promise.resolve(input)
        }
    }

    function lookupUser(input: dt.UserCreateInput): Promise<dt.UserCreateInput> {
        return server.db(dbCollection).findOne({ name: input.name })
            .then<dt.User | null>((x) => x)
            .then<dt.UserCreateInput>((x) => x ? Promise.reject({ code: 409, message: 'user already exists' }) : input)
    }

    function createUser(input: dt.UserCreateInput): Promise<dt.UserCreateOutput> {
        const passwordHash = bcrypt.hashSync(input.password, 10)
        const token = generateLoginToken()
        const tokenHash = bcrypt.hashSync(token, 10)
        const user: dt.User = { name: input.name, passwordHash, tokenHash, decks: [] }
        const result: dt.UserCreateOutput = { token }
        return server.db(dbCollection).insertOne(user).then(() => result)
    }
}

export function userDelete(rawInput: server.RawInput, _: server.Context, callback: server.Callback) {
    return server.run('user delete', rawInput, callback, run)

    function run(input: dt.UserDeleteInput): Promise<dt.UserDeleteOutput> {
        return validateInput(input)
            .then(lookupUser)
            .then(checkTokenAndPassword)
            .then(deleteUser)
    }

    function validateInput(input: dt.UserDeleteInput): Promise<dt.UserDeleteInput> {
        if (input.name === undefined) {
            return Promise.reject({ code: 400, message: '"name" parameter not specified' })
        } else if (input.token === undefined) {
            return Promise.reject({ code: 400, message: '"token" parameter not specified' })
        } else if (input.password === undefined) {
            return Promise.reject({ code: 400, message: '"password" parameter not specified' })
        } else {
            return Promise.resolve(input)
        }
    }

    interface LookupUserRet { input: dt.UserDeleteInput, user: dt.User }
    function lookupUser(input: dt.UserDeleteInput): Promise<LookupUserRet> {
        return findUser(input.name).then((x) => ({ input, user: x }))
    }

    function checkTokenAndPassword(data: LookupUserRet): Promise<dt.User> {
        return bcrypt.compare(data.input.token, data.user.tokenHash).then((tokenMatch) => {
            if (tokenMatch) {
                return bcrypt.compare(data.input.password, data.user.passwordHash)
                    .then((passwordMatch) => {
                        if (passwordMatch) {
                            return data.user
                        } else {
                            return Promise.reject<dt.User>({ code: 403, message: 'password was incorrect' })
                        }
                    })
            } else {
                return Promise.reject<dt.User>({ code: 403, message: 'not logged in' })
            }
        })
    }

    function deleteUser(user: dt.User): Promise<dt.UserDeleteOutput> {
        return server.db(dbCollection).deleteOne({ name: user.name }).then(() => ({}))
    }
}

export function userUpdate(rawInput: server.RawInput, _: server.Context, callback: server.Callback) {
    return server.run('user update', rawInput, callback, run)

    function run(input: dt.UserUpdateInput): Promise<dt.UserUpdateOutput> {
        return validateInput(input)
            .then(lookupUser)
            .then(checkToken)
            .then(updateUser)
    }

    function validateInput(input: dt.UserUpdateInput): Promise<dt.UserUpdateInput> {
        if (input.name === undefined) {
            return Promise.reject({ code: 400, message: '"name" parameter not specified' })
        } else if (input.token === undefined) {
            return Promise.reject({ code: 400, message: '"token" parameter not specified' })
        } else {
            return Promise.resolve(input)
        }
    }

    interface LookupUserRet { input: dt.UserUpdateInput, user: dt.User }
    function lookupUser(input: dt.UserUpdateInput): Promise<LookupUserRet> {
        return findUser(input.name).then((x) => ({ input, user: x }))
    }

    interface CheckTokenRet { input: dt.UserUpdateInput, user: dt.User }
    function checkToken(data: LookupUserRet): Promise<CheckTokenRet> {
        return bcrypt.compare(data.input.token, data.user.tokenHash)
            .then((match) => {
                if (match) {
                    return data
                } else {
                    return Promise.reject<CheckTokenRet>({ code: 403, message: 'not logged in' })
                }
            })
    }

    function updateUser(data: CheckTokenRet): Promise<dt.UserUpdateOutput> {
        let setPasswordHash: string | undefined
        let setDecks: dt.Deck[] | undefined

        if (data.input.password) {
            console.log('updating password')
            setPasswordHash = bcrypt.hashSync(data.input.password, 10)
        }

        if (data.input.removeDeckNames) {
            console.log('removing decks')
            const deckNamesToRemove = data.input.removeDeckNames
            setDecks = setDecks ? setDecks : data.user.decks
            setDecks = setDecks.filter((x) => !deckNamesToRemove.some((y) => x.name === y))
        }

        if (data.input.addDecks) {
            console.log('adding decks')
            setDecks = setDecks ? setDecks : data.user.decks
            setDecks = setDecks.concat(data.input.addDecks)
            for (let i = 0; i < setDecks.length - 1; i++) {
                for (let j = i + 1; j < setDecks.length; j++) {
                    if (setDecks[i].name === setDecks[j].name) {
                        return Promise.reject<dt.UserUpdateOutput>({ code: 400, message: 'duplicate deck name' })
                    }
                }
            }
        }

        const toSet: any = {}
        if (setPasswordHash) {
            toSet.passwordHash = setPasswordHash
        }
        if (setDecks) {
            toSet.decks = setDecks
        }
        return server.db(dbCollection).updateOne({ name: data.user.name }, { $set: toSet }).then(() => ({}))
    }
}
