import * as mongo from 'mongodb'

const dbUri = process.env.DONKEYTOWNSFOLK_MONGODB_URI!
if (!dbUri) {
    throw new Error('DONKEYTOWNSFOLK_MONGODB_URI not set')
}

export interface RawInput {
    body: string
    // we only care about the body - the rest of the fields (header, etc) are excluded
}

export interface RawOutput {
    statusCode: number
    headers: {}
    body: string
}

export interface Callback {
    (error: Error): void
    (error: undefined, result: RawOutput): void
}

export interface Context {
    functionName: string
    functionVersion: string
    invokedFunctionArn: string
    memoryLimitInMB: number
    awsRequestId: string
    logGroupName: string
    logStreamName: string
    succeed(result?: object): void
    fail(error?: Error): void
    done(error?: Error, result?: object): void
    getRemainingTimeInMillis(): number
}

export type Handler<TInput, TOutput> = (input: TInput) => Promise<TOutput>

let dbConnection: { client: mongo.MongoClient, db: mongo.Db }

export function db(collection: string): mongo.Collection {
    if (!dbConnection) {
        throw new Error('DB not initialized')
    }
    return dbConnection.db.collection(collection)
}

export function run<TInput, TOutput>(
    name: string,
    rawInput: RawInput,
    callback: Callback,
    handler: Handler<TInput, TOutput>) {

    console.log(`function invoked: ${name}`)

    const headers = {
        'Access-Control-Allow-Origin': '*', // Required for CORS support to work
        'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
    }

    // treat invalid requests or requests without a 'body' parameter as having an empty body
    if (rawInput.body === undefined) {
        rawInput = { body: '{}' }
    }

    const input = JSON.parse(rawInput.body) as TInput
    mongo.MongoClient.connect(dbUri)
        .then((x) => dbConnection = { client: x, db: x.db() })
        .then(() => handler(input))
        .then((result) => {
            console.log(`${name}: success`)
            dbConnection.client.close()
            callback(undefined, {
                statusCode: 200,
                headers,
                body: JSON.stringify(result),
            })
        })
        .catch((e) => {
            dbConnection.client.close()
            if (e.code && e.message) {
                console.error(`${name}: error ${e.code}: ${e.message}`)
                callback(undefined, {
                    statusCode: e.code,
                    headers,
                    body: e.message,
                })
            } else {
                console.error(e)
                callback(e)
            }
        })
}

// Note that there are no type annotations available for bottleneck as of yet, so I'm using the JS raw...
// tslint:disable-next-line:no-var-requires
const Bottleneck = require('bottleneck')
export class RateLimiter {
    private readonly limiter: any

    constructor(readonly outstandingRequests: number, readonly timeBetweenRequestsMs: number) {
        this.limiter = new Bottleneck(outstandingRequests, timeBetweenRequestsMs)
    }

    public schedule<R>(fn: () => PromiseLike<R>): Promise<R> {
        return this.limiter.schedule(fn)
    }
}
