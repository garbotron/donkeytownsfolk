import * as bcrypt from 'bcryptjs'
import * as dt from '../common/model'
import * as server from './common'

const dbCollection = process.env.DONKEYTOWNSFOLK_MONGODB_COLLECTION_USERS!
if (!dbCollection) {
    throw new Error('DONKEYTOWNSFOLK_MONGODB_COLLECTION_USERS not set')
}

function findUser(name: string): Promise<dt.User | undefined> {
    return server.db(dbCollection).findOne({ name }).then((x) => x || undefined)
}

function sortCardsInDeck(deck: dt.Deck) {
    // sort by: basic lands first (alphabetically), then everything else alphabetically
    deck.cards = deck.cards.sort(compareCards)
    deck.sideboard = deck.sideboard.sort(compareCards)
    function compareCards(a: dt.Card, b: dt.Card): number {
        if (a.lastScanPrice === 0 && b.lastScanPrice !== 0) {
            return -1
        } else if (a.lastScanPrice !== 0 && b.lastScanPrice === 0) {
            return 1
        } else {
            const aName = a.name.toLowerCase()
            const bName = b.name.toLowerCase()
            return aName === bName ? 0 : (aName < bName ? -1 : 1)
        }
    }
}

function sortDecks(decks: dt.DeckWithUser[]): dt.DeckWithUser[] {
    // sort by: deck name first, then username alphabetically
    return decks.sort(compareDecks)
    function compareDecks(a: dt.DeckWithUser, b: dt.DeckWithUser): number {
        const aDeck = a.deck.name.toLowerCase()
        const bDeck = b.deck.name.toLowerCase()
        if (aDeck === bDeck) {
            const aUser = a.user.toLowerCase()
            const bUser = b.user.toLowerCase()
            return aUser === bUser ? 0 : (aUser < bUser ? -1 : 1)
        } else {
            return aDeck === bDeck ? 0 : (aDeck < bDeck ? -1 : 1)
        }
    }
}

export function decksQuery(rawInput: server.RawInput, _: server.Context, callback: server.Callback) {
    return server.run('decks query', rawInput, callback, run)

    function run(input: dt.DecksQueryInput): Promise<dt.DecksQueryOutput> {
        return validateInput(input)
            .then(checkLoggedInUser)
            .then(findDecks)
    }

    function validateInput(input: dt.DecksQueryInput): Promise<dt.DecksQueryInput> {
        // there's no such thing as invalid input to this function (all parameters are optional)
        return Promise.resolve(input)
    }

    interface CheckLoggedInUserRet { input: dt.DecksQueryInput, user: dt.User | undefined }
    function checkLoggedInUser(input: dt.DecksQueryInput): Promise<CheckLoggedInUserRet> {
        if (!input.loggedInUser || !input.loggedInUser.name || !input.loggedInUser.token) {
            return Promise.resolve({ input, user: undefined })
        } else {
            const [userName, token] = [input.loggedInUser.name, input.loggedInUser.token]
            return findUser(userName)
                .then((user) => {
                    if (user) {
                        return bcrypt.compare(token, user.tokenHash).then((match) => match ? user : undefined)
                    } else {
                        return undefined
                    }
                })
                .then((user) => ({ input, user }))
        }
    }

    function findDecks(data: CheckLoggedInUserRet): Promise<dt.DecksQueryOutput> {
        return server.db(dbCollection).find().toArray()
            .then((scanOutput) => {
                const output: dt.DecksQueryOutput = { decks: [] }
                for (const user of scanOutput as dt.User[]) {
                    for (const deck of user.decks) {
                        if (shouldIncludeDeck(data.input, data.user, user, deck)) {
                            sortCardsInDeck(deck)
                            output.decks.push({ user: user.name, deck })
                        }
                    }
                }
                output.decks = sortDecks(output.decks)
                return output
            })
    }

    function shouldIncludeDeck(
        input: dt.DecksQueryInput,
        loggedInUser: dt.User | undefined,
        user: dt.User,
        deck: dt.Deck) {

        if (deck.isPrivate && (!loggedInUser || loggedInUser.name !== user.name)) {
            return false // this user doesn't have permission to view this private deck
        }
        if (input.filterUser && input.filterUser !== user.name) {
            return false // we're filtering a specific user's decks and this one doesn't match
        }
        if (input.filterQuery) {
            const queryString = `${user.name.toLowerCase()} ${deck.name.toLowerCase()}`
            if (queryString.indexOf(input.filterQuery.toLowerCase()) < 0) {
                return false
            }
        }
        return true
    }
}
